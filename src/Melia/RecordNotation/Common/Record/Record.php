<?php

namespace Melia\RecordNotation\Common\Record;

use Melia\Uuid\Common\Uuid\UuidAwareInterface;

/**
 * Interface of Record
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Record extends UuidAwareInterface {

    /**
     * Retrieve data
     *
     * @return mixed
     */
    public function getData();
}