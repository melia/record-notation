<?php

namespace Melia\RecordNotation\Common\Record\Factory;

use Melia\RecordNotation\Common\Record\Record;

/**
 * Interface of Factory
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Factory {

    /**
     * Get default record generation context
     *
     * @return RecordGenerationContext
     */
    public function getDefaultRecordGenerationContext();

    /**
     * Create record
     *
     * @param mixed $data
     * @param string $uuid
     * @param RecordGenerationContext $context
     * @return Record
     */
    public function create($data, $uuid = null, RecordGenerationContext $context = null);
}