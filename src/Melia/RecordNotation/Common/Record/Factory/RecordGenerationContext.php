<?php

namespace Melia\RecordNotation\Common\Record\Factory;

use Melia\Uuid\Common\Uuid\Generator\GeneratorInterface;

/**
 * Interface of RecordGenerationContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface RecordGenerationContext {

    /**
     * Get uuid generator
     *
     * @return GeneratorInterface
     */
    public function getUuidGenerator();
}