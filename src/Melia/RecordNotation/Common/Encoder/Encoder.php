<?php

namespace Melia\RecordNotation\Common\Encoder;

use Melia\RecordNotation\Common\Record\Record;

/**
 * Interface of Encoder
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Encoder {

    /**
     * Encode record
     *
     * @param Record $record
     * @return mixed
     */
    public function encode(Record $record);
}