<?php

namespace Melia\RecordNotation\Common\Encoder;

/**
 * Interface of EncoderAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface EncoderAwareInterface {

    /**
     * Retrieve encoder
     *
     * @return Encoder
     */
    public function getEncoder();
}