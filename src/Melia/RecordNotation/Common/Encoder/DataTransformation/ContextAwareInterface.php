<?php

namespace Melia\RecordNotation\Common\Encoder\DataTransformation;

/**
 * Interface of ContextAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface ContextAwareInterface {

    /**
     * Get data conversion context
     *
     * @return Context
     */
    public function getContext();
}