<?php

namespace Melia\RecordNotation\Common\Encoder\DataTransformation;

use Melia\RecordNotation\Common\Record\Record;

/**
 * Interface of Context
 *
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Context {

    /**
     * Determine wether record is supported
     *
     * @param Record $record
     * @return boolean
     */
    public function isSupportedRecord(Record $record);

    /**
     * Transforms data to a useable data presentation
     *
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}