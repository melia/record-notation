<?php

namespace Melia\RecordNotation\Common\Decoder;

/**
 * Interface of DecoderAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface DecoderAwareInterface {

    /**
     * Retrieve decoder
     *
     * @return Decoder
     */
    public function getDecoder();
}