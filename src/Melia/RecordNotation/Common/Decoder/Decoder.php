<?php

namespace Melia\RecordNotation\Common\Decoder;

use Melia\RecordNotation\Common\Record\Record;
use Melia\RecordNotation\Common\Scheme\Scheme;

/**
 * Interface of Decoder
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Decoder {

    /**
     * Decode data
     *
     * @param mixed $data
     * @return Record
     */
    public function decode($data);

    /**
     * Determine wether data has supported scheme
     *
     * @param Scheme $scheme
     * @return boolean
     */
    public function isSupportedScheme(Scheme $scheme);
}