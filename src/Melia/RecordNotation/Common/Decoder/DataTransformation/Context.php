<?php

namespace Melia\RecordNotation\Common\Decoder\DataTransformation;

/**
 * Interface of Context
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Context {

    /**
     * Determine wether data is supported
     *
     * @param mixed $data
     * @return boolean
     */
    public function isSupportedData($data);

    /**
     * Transforms data to a useable data presentation
     *
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}