<?php

namespace Melia\RecordNotation\Common\Decoder\DataTransformation;

/**
 * Interface of ContextAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface ContextAwareInterface {

    /**
     * Get data transformation context
     *
     * @return Context
     */
    public function getContext();
}