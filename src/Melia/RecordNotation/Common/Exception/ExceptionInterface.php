<?php

namespace Melia\RecordNotation\Common\Exception;

/**
 * Interface of ExceptionInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface ExceptionInterface {
}