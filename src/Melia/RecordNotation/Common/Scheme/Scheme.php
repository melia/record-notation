<?php

namespace Melia\RecordNotation\Common\Scheme;

use Melia\RecordNotation\Common\Scheme\Template\Wildcards;

/**
 * Interface of Scheme
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Scheme extends Wildcards {

    /**
     * Retrieve the scheme version
     *
     * @return integer
     */
    public function getVersion();

    /**
     * Retrieve template
     *
     * @return string
     */
    public function getTemplate();

    /**
     * Retrieve the checksum algorithm name
     *
     * @return string
     */
    public function getChecksumAlgorithmName();

    /**
     * Retrieve the uuid version
     *
     * @return string
     */
    public function getUUIDVersion();

    /**
     * Retrieve regular expression for processing (parsing/validation) related record
     *
     * @return string
     */
    public function getRegularExpression();
}