<?php

namespace Melia\RecordNotation\Common\Scheme\Template;

/**
 * Interface of Wildcards
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Wildcards {
    /**
     * Wildcard for version inschemeion
     *
     * @var string
     */
    const WILDCARD_VERSION = "%version";
    /**
     * Wildcard for uuid inschemeion
     *
     * @var string
     */
    const WILDCARD_UUID = "%uuid";
    /**
     * Wildcard for checksum inschemeion
     *
     * @var string
     */
    const WILDCARD_CHECKSUM = "%checksum";
    /**
     * Wildcard for data
     *
     * @var string
     */
    const WILDCARD_DATA = "%data";
}