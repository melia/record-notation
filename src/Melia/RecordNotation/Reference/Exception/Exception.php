<?php

namespace Melia\RecordNotation\Reference\Exception;

use \Exception as PHPCoreException;
use Melia\RecordNotation\Common\Exception\ExceptionInterface;

/**
 * Implementation of Exception
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Exception extends PHPCoreException implements ExceptionInterface {
}