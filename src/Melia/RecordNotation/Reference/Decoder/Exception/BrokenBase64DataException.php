<?php

namespace Melia\RecordNotation\Reference\Decoder\Exception;

/**
 * Implementation of BrokenBase64DataException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class BrokenBase64DataException extends Exception {
}