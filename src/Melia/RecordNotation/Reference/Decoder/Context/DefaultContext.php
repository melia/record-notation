<?php

namespace Melia\RecordNotation\Reference\Decoder\Context;

use Melia\RecordNotation\Common\Decoder\DataTransformation\Context;
use Melia\RecordNotation\Reference\Validator\Validator;

/**
 * Implementation of DefaultContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class DefaultContext implements Context {

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Decoder\DataTransformation\Context::isSupportedData()
     */
    public function isSupportedData($data) {
        return Validator::isSerialized($data);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Decoder\DataTransformation\Context::transform()
     */
    public function transform($data) {
        return unserialize($data);
    }
}