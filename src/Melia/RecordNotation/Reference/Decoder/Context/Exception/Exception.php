<?php

namespace Melia\RecordNotation\Reference\Decoder\Context\Exception;

use Melia\RecordNotation\Reference\Decoder\Exception\Exception as BaseException;

/**
 * Implementation of Exception
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Exception extends BaseException {
}