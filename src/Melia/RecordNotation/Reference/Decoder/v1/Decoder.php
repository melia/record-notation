<?php

namespace Melia\RecordNotation\Reference\Decoder\v1;

use Melia\RecordNotation\Common\Decoder\Decoder as DecoderInterface;
use Melia\RecordNotation\Common\Scheme\Scheme;
use Melia\RecordNotation\Reference\Record\Record;
use Melia\RecordNotation\Reference\Decoder\Context\Exception\UnsupportedDataException;
use Melia\RecordNotation\Reference\Decoder\Exception\Exception;
use Melia\RecordNotation\Common\Decoder\DataTransformation\Context;
use Melia\RecordNotation\Common\Decoder\DataTransformation\ContextAwareInterface;
use Melia\RecordNotation\Reference\Validator\Validator;
use Melia\RecordNotation\Reference\Decoder\Exception\BrokenBase64DataException;
use Melia\RecordNotation\Reference\Decoder\Context\DefaultContext;

/**
 * Implementation of Decoder
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Decoder implements DecoderInterface, ContextAwareInterface {
    /**
     * Data transformation context
     *
     * @var Context
     */
    private $context;

    /**
     * Constructor
     *
     * @param Context $context
     */
    public function __construct(Context $context = null) {
        if(null === $context) {
            $context = new DefaultContext();
        }
        $this->setContext($context);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Decoder\DataTransformation\ContextAwareInterface::getContext()
     */
    public function getContext() {
        return $this->context;
    }

    /**
     * Set data transformation context
     *
     * @param Context $context
     * @return \Melia\RecordNotation\Reference\Decoder\v1\Decoder
     */
    public function setContext(Context $context) {
        $this->context = $context;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Decoder\Decoder::isSupportedScheme()
     */
    public function isSupportedScheme(Scheme $scheme) {
        return ($scheme instanceof \Melia\RecordNotation\Reference\Scheme\v1\Scheme);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Decoder\Decoder::decode()
     */
    public function decode($data) {
        // capture data fragments
        $capturedRecordElements = $this->captureRecordElements($data);
        // validate base64 data consistency
        if(Validator::isConsistentBase64($base64DataPresentation = $capturedRecordElements[3], $capturedRecordElements[2], \Melia\RecordNotation\Reference\Scheme\v1\Scheme::CHECKSUM_ALGORITHM_NAME)) {
            // decoding of base64 is handle by the encoder as there is no need for external base64 decoding implementations
            if($this->getContext()->isSupportedData($partialDecodedData = base64_decode($base64DataPresentation), true)) {
                return new Record($this->getContext()->transform($partialDecodedData), $capturedRecordElements[1]);
            } else {
                throw new UnsupportedDataException(sprintf("Unsupported data has been detected: %s", var_export($partialDecodedData, true)));
            }
        } else {
            throw new BrokenBase64DataException(sprintf("Broken base64 data has been detected: %s", var_export($base64DataPresentation, true)));
        }
    }

    /**
     * Capture record elements
     *
     * @param string $data
     * @throws Exception
     * @throws UnsupportedDataException
     * @return array
     */
    private function captureRecordElements($data) {
        // should be already reviewed by the context implementation
        if(is_string($data)) {
            if(false !== preg_match(\Melia\RecordNotation\Reference\Scheme\v1\Scheme::REGULAR_EXPRESSION, $data, $capturedRecordElements)) {
                return $capturedRecordElements;
            } else {
                throw new Exception(sprintf("Unable to capture record elements of data: %s", var_export($data, true)));
            }
        } else {
            throw new UnsupportedDataException(sprintf("Unsupported data has been detected: %s", var_export($data, true)));
        }
    }
}