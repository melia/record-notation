<?php

namespace Melia\RecordNotation\Reference\Scheme\v1;

use Melia\RecordNotation\Common\Scheme\Scheme as SchemeInterface;

/**
 * Implementation of Scheme
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Scheme implements SchemeInterface {
    /**
     * Scheme version
     *
     * @var integer
     */
    const VERSION = 1;
    /**
     * UUID version
     *
     * @var integer
     */
    const VERSION_UUID = 5;
    /**
     * Template
     *
     * @var string
     */
    const TEMPLATE = "[%version|%uuid|%checksum|%data]";
    /**
     * Checksum algorithm name
     *
     * @var string
     */
    const CHECKSUM_ALGORITHM_NAME = "md5";
    /**
     * Regular expression
     *
     * @var string
     */
    const REGULAR_EXPRESSION = "/\[1\|([a-z0-9]{8}-(?:[a-z0-9]{4}-){3}[a-z0-9]{12})\|([a-z0-9]{32})\|([a-zA-Z0-9\+=\/]+)\]/";

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Scheme\Scheme::getVersion()
     */
    public function getVersion() {
        return self::VERSION;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Scheme\Scheme::getTemplate()
     */
    public function getTemplate() {
        return self::TEMPLATE;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Scheme\Scheme::getChecksumAlgorithmName()
     */
    public function getChecksumAlgorithmName() {
        return self::CHECKSUM_ALGORITHM_NAME;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Scheme\Scheme::getUUIDVersion()
     */
    public function getUUIDVersion() {
        return self::VERSION_UUID;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Scheme\Scheme::getRegularExpression()
     */
    public function getRegularExpression() {
        return self::REGULAR_EXPRESSION;
    }
}