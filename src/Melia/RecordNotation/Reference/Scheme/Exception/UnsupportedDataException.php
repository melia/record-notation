<?php

namespace Melia\RecordNotation\Reference\Scheme\Exception;

/**
 * Implementation of UnsupportedDataException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedDataException extends Exception {
}