<?php

namespace Melia\RecordNotation\Reference\Scheme\Exception;

/**
 * Implementation of UnknownSchemeException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnknownSchemeException extends Exception {
}