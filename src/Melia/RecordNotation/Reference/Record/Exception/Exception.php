<?php

namespace Melia\RecordNotation\Reference\Record\Exception;

use Melia\RecordNotation\Reference\Exception\Exception as BaseException;

/**
 * Implementation of Exception
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Exception extends BaseException {
}