<?php

namespace Melia\RecordNotation\Reference\Record\Exception;

/**
 * Implementation of UnsupportedUUIDException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedUUIDException extends Exception {
}