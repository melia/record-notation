<?php

namespace Melia\RecordNotation\Reference\Record;

use Melia\RecordNotation\Common\Record\Record as RecordInterface;
use Melia\RecordNotation\Reference\Record\Exception\UnsupportedUUIDException;
use Melia\Uuid\Reference\Validator\Validator;

/**
 * Implementation of Record
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Record implements RecordInterface {
    /**
     * UUID
     *
     * @var string
     */
    private $uuid;
    /**
     * Data
     *
     * @var mixed
     */
    private $data;

    /**
     * Constructor
     *
     * @param mixed $data
     * @param string $uuid
     */
    public function __construct($data, $uuid) {
        $this->setData($data)->setUUID($uuid);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Record\Record::getUUID()
     */
    public function getUUID() {
        return $this->uuid;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @throws UnsupportedUUIDException
     * @return \Melia\RecordNotation\Reference\Record\Record
     */
    public function setUUID($uuid) {
        if(Validator::isSupportedUUID($uuid)) {
            $this->uuid = $uuid;
        } else {
            throw new UnsupportedUUIDException(sprintf("Unsupported UUID has been detected: %s", var_export($uuid, true)));
        }
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Record\Record::getData()
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Set data
     *
     * @param mixed $data
     * @return \Melia\RecordNotation\Reference\Record\Record
     */
    public function setData($data) {
        $this->data = $data;
        return $this;
    }
}