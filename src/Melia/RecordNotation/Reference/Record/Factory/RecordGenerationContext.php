<?php

namespace Melia\RecordNotation\Reference\Record\Factory;

use Melia\Uuid\Common\Uuid\Generator\GeneratorInterface;
use Melia\RecordNotation\Common\Record\Factory\RecordGenerationContext as RecordGenerationContextInterface;

/**
 * Implementation of RecordGenerationContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class RecordGenerationContext implements RecordGenerationContextInterface {
    /**
     * Uuid generator
     *
     * @var GeneratorInterface
     */
    private $uuidGenerator;

    /**
     * Constructor
     *
     * @param GeneratorInterface $uuidGenerator
     */
    public function __construct(GeneratorInterface $uuidGenerator) {
        $this->setUuidGenerator($uuidGenerator);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Record\Factory\RecordGenerationContext::getUuidGenerator()
     */
    public function getUuidGenerator() {
        return $this->uuidGenerator;
    }

    /**
     * Set uuid generator
     *
     * @param GeneratorInterface $uuidGenerator
     * @return \Melia\RecordNotation\Reference\Record\Factory\RecordGenerationContext
     */
    public function setUuidGenerator(GeneratorInterface $uuidGenerator) {
        $this->uuidGenerator = $uuidGenerator;
        return $this;
    }
}