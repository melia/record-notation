<?php

namespace Melia\RecordNotation\Reference\Record\Factory;

use Melia\RecordNotation\Common\Record\Factory\Factory as FactoryInterface;
use Melia\RecordNotation\Common\Record\Factory\RecordGenerationContext;
use Melia\RecordNotation\Reference\Record\Record;

/**
 * Implementation of Factory
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Factory implements FactoryInterface {
    /**
     * Record generation context
     *
     * @var RecordGenerationContext
     */
    private $defaultRecordGenerationContext;

    /**
     * Constructor
     *
     * @param RecordGenerationContext $context
     */
    public function __construct(RecordGenerationContext $context) {
        $this->setDefaultRecordGenerationContext($context);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Record\Factory\Factory::getDefaultRecordGenerationContext()
     */
    public function getDefaultRecordGenerationContext() {
        return $this->defaultRecordGenerationContext;
    }

    /**
     * Set default record generation context
     *
     * @param RecordGenerationContext $context
     * @return \Melia\RecordNotation\Reference\Record\Factory\Factory
     */
    public function setDefaultRecordGenerationContext(RecordGenerationContext $context) {
        $this->defaultRecordGenerationContext = $context;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Record\Factory\Factory::create()
     */
    public function create($data, $uuid = null, RecordGenerationContext $context = null) {
        if(null === $context) {
            $context = $this->getDefaultRecordGenerationContext();
        }
        if(null === $uuid) {
            $uuid = $context->getUuidGenerator()->generate();
        }
        return new Record($data, $uuid);
    }
}