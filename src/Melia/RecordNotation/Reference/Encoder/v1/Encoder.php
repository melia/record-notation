<?php

namespace Melia\RecordNotation\Reference\Encoder\v1;

use Melia\RecordNotation\Common\Encoder\Encoder as EncoderInterface;
use Melia\RecordNotation\Common\Record\Record;
use Melia\RecordNotation\Reference\Scheme\v1\Scheme;
use Melia\RecordNotation\Reference\Validator\Validator;
use Melia\RecordNotation\Reference\Encoder\Exception\BrokenBase64DataException;
use Melia\RecordNotation\Reference\Encoder\Exception\UnsupportedRecordException;
use Melia\RecordNotation\Common\Encoder\DataTransformation\ContextAwareInterface;
use Melia\RecordNotation\Common\Encoder\DataTransformation\Context;
use Melia\RecordNotation\Reference\Encoder\Context\DefaultContext;

/**
 * Implementation of Encoder
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Encoder implements EncoderInterface, ContextAwareInterface {
    /**
     * Data transformation context
     *
     * @var Context
     */
    private $context;

    /**
     * Constructor
     *
     * @param Context $context
     */
    public function __construct(Context $context = null) {
        if(null === $context) {
            $context = new DefaultContext();
        }
        $this->setContext($context);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Encoder\DataTransformation\ContextAwareInterface::getContext()
     */
    public function getContext() {
        return $this->context;
    }

    /**
     * Set data transformation context
     *
     * @param Context $context
     * @return \Melia\RecordNotation\Reference\Encoder\v1\Encoder
     */
    public function setContext(Context $context) {
        $this->context = $context;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Encoder\Encoder::encode()
     */
    public function encode(Record $record) {
        return strtr(Scheme::TEMPLATE, $this->generateReplacements($record));
    }

    /**
     * Generate a list of replacement
     *
     * @param Record $record
     * @return array
     */
    private function generateReplacements(Record $record) {
        return array(
                Scheme::WILDCARD_VERSION => (string)Scheme::VERSION,
                Scheme::WILDCARD_CHECKSUM => $this->generateChecksum($base64DataPresentation = $this->getBase64DataPresentation($record)),
                Scheme::WILDCARD_UUID => $record->getUUID(),
                Scheme::WILDCARD_DATA => $base64DataPresentation);
    }

    /**
     * Get base64 data presentation of record
     *
     * @param Record $record
     * @throws BrokenBase64DataException
     * @throws UnsupportedRecordException
     * @return string
     */
    private function getBase64DataPresentation(Record $record) {
        if($this->getContext()->isSupportedRecord($record)) {
            if(Validator::isBase64($base64DataPresentation = $this->getContext()->transform($record->getData()))) {
                return $base64DataPresentation;
            } else {
                throw new BrokenBase64DataException("Broken base64 data presentation has been detected");
            }
        } else {
            throw new UnsupportedRecordException("Unsupported record has been detected");
        }
    }

    /**
     * Generate checksum
     *
     * @param string $encodedData
     * @return string
     */
    private function generateChecksum($encodedData) {
        return hash(Scheme::CHECKSUM_ALGORITHM_NAME, $encodedData);
    }
}