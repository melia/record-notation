<?php

namespace Melia\RecordNotation\Reference\Encoder\Context;

use Melia\RecordNotation\Common\Encoder\DataTransformation\Context;
use Melia\RecordNotation\Common\Record\Record;

/**
 * Implementation of DefaultContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class DefaultContext implements Context {

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Encoder\DataTransformation\Context::isSupportedRecord()
     */
    public function isSupportedRecord(Record $record) {
        return true;
    }

    /**
     *
     * {@inheritdoc}
     * @see \Melia\RecordNotation\Common\Encoder\DataTransformation\Context::transform()
     */
    public function transform($data) {
        // TODO review data serialization
        return base64_encode(serialize($data));
    }
}