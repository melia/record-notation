<?php

namespace Melia\RecordNotation\Reference\Encoder\Exception;

/**
 * Implementation of UnsupportedRecordException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedRecordException extends Exception {
}