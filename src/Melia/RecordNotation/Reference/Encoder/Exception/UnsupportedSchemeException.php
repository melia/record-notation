<?php

namespace Melia\RecordNotation\Reference\Encoder\Exception;

/**
 * Implementation of UnsupportedSchemeException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedSchemeException extends Exception {
}