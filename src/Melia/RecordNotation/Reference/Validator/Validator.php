<?php

namespace Melia\RecordNotation\Reference\Validator;

/**
 * Implementation of Validator
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class Validator {

    /**
     * Determine wether data has base64 encoding
     *
     * @param string $data
     * @return boolean
     */
    public static function isBase64($data) {
        return is_string($data) && (1 === preg_match("/[a-zA-Z0-9\+=\/]+/", $data));
    }

    /**
     * Determine wether data is serialized
     *
     * @param string $data
     * @param mixed $unserialized
     * @return boolean
     */
    public static function isSerialized($data, &$unserialized = null) {
        return ((false !== $unserialized = @unserialize($data)) || ("b:0;" === $data));
    }

    /**
     * Determine wether hash algorith name is supported
     *
     * @param string $hashAlgorithmName
     * @return boolean
     */
    public static function isSupportedHashAlgorithName($hashAlgorithmName) {
        return in_array($hashAlgorithmName, hash_algos(), true);
    }

    /**
     * Determine wether data is consistent
     *
     * @param string $data
     * @param string $checksum
     * @param string $checksumAlgorithName
     * @return boolean
     */
    public static function isConsistentData($data, $checksum, $checksumAlgorithName) {
        return is_string($data) && is_string($checksum) && static::isSupportedHashAlgorithName($checksumAlgorithName) && ($checksum === hash($checksumAlgorithName, $data));
    }

    /**
     * Determine wether data is consistent base64
     *
     * @param string $data
     * @param string $checksum
     * @param string $checksumAlgorithName
     * @return boolean
     */
    public static function isConsistentBase64($data, $checksum, $checksumAlgorithName) {
        return static::isBase64($data) && static::isConsistentData($data, $checksum, $checksumAlgorithName);
    }
}