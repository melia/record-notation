<?php
use Melia\RecordNotation\Reference\Encoder\v1\Encoder;
use Melia\RecordNotation\Reference\Record\Factory\Factory;
use Melia\RecordNotation\Reference\Record\Factory\RecordGenerationContext;
use Melia\Uuid\Reference\Uuid\Generator\UuidGenerationContext;
use Melia\Uuid\Reference\Uuid\Generator\Generator;
use Melia\RecordNotation\Reference\Decoder\v1\Decoder;
// setup autoloading
require_once '../vendor/autoload.php';
// initialize encoder v1
$encoder = new Encoder();
// initialize decoder v1
$decoder = new Decoder();
// initialize factory for record generation
$factory = new Factory(new RecordGenerationContext(new Generator(new UuidGenerationContext(4))));
// create record from array data
$record = $factory->create(array(
        "username" => "melia",
        "email" => "marvin_elia@web.de"));
// encode record
$encodedRecord = $encoder->encode($record);
// decode record
$decodedRecord = $decoder->decode($encodedRecord);
// export decoded record for output
$exportDecodedRecord = var_export($decodedRecord, true);
// output of encoded + decoded record
echo <<<OUTPUT
<pre>
Encoded record:
{$encodedRecord}

Decoded record:
{$exportDecodedRecord}
</pre>
OUTPUT;
